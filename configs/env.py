import enum
import os

from pydantic import BaseSettings, Field


class Env(str, enum.Enum):
    """Common environment names"""

    DEV = 'dev'
    TEST = 'test'
    QA = 'qa'
    LOCAL = 'local'
    PROD = 'prod'


class EnvSettings(BaseSettings):
    config_env: Env = Field(description='Please export CONFIG_ENV=..')
    app_dir: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    @property
    def path(self):
        return os.path.join(self.app_dir, 'configs', f'{self.config_env}.env')


env_settings = EnvSettings()
