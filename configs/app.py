from typing import Optional


from pydantic import FilePath, BaseSettings

from configs.env import env_settings


class AppSettings(BaseSettings):
    APP_NAME: str = 'reply_api'
    SERVICE_NAME: str = 'ReplyApi'
    DESCRIPTION: str = 'Threads & Comments API'
    DEBUG: bool = False
    LOGGER_LEVEL: str = 'INFO'

    # configs for Mongo
    MONGO_DSN: str

    class Config:
        env_file = env_settings.path


app_settings = AppSettings()
