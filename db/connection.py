import asyncio

from beanie import init_beanie
from motor.core import AgnosticClient, AgnosticDatabase
from motor.motor_asyncio import AsyncIOMotorClient

from base.singleton import Singleton
from db.mongo_db.model_manager import model_register


class MongoConnection(metaclass=Singleton):
    def __init__(
        self,
        mongo_dsn: str,
    ):
        self.mongo_dsn: str = mongo_dsn
        self.client: AsyncIOMotorClient = AsyncIOMotorClient
        self.client.get_io_loop = asyncio.get_event_loop
        self.connection: AgnosticClient | None = None
        self.db: AgnosticDatabase | None = None

    async def init_mongo_db(self):
        self.connection = self.client(self.mongo_dsn)
        self.db = self.connection[self.connection.get_database().name]
        await init_beanie(
            database=self.db,
            document_models=model_register.models,
        )

    def close(self):
        if self.connection:
            self.connection.close()
