import logging
from datetime import datetime

from beanie import Document, PydanticObjectId, Link
from bson import ObjectId

from db.mongo_db.model_manager import model_register

logger = logging.getLogger(__name__)


@model_register
class CommentDocument(Document):
    thread_id: ObjectId
    msg: str
    created_at: datetime
    updated_at: datetime

    class Settings:
        name = 'comments'

    class Config:
        arbitrary_types_allowed = True
        validate_assignment = True


@model_register
class ThreadDocument(Document):
    parent_id: str | None
    replied_to: PydanticObjectId | None = None
    updated_at: datetime
    resolved: bool = False
    comments: list[Link[CommentDocument]] = []
    comments_count: int = 0

    class Settings:
        name = 'threads'

    class Config:
        arbitrary_types_allowed = True
        validate_assignment = True
