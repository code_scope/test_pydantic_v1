from beanie import Document

from base.singleton import Singleton


class MongoModelsRegister(metaclass=Singleton):
    """
    Регистратор моделей mongo_db
    Регистрация моделей необходима для инициализации моделей в ODM 'beanie'
    """

    __registered_models: set[Document] = set()

    def __call__(self, cls: Document) -> Document:
        if issubclass(cls, Document):
            self.__registered_models.add(cls)
        return cls

    @property
    def models(self) -> list[Document]:
        return list(self.__registered_models)


model_register = MongoModelsRegister()
