import logging

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from api.routers import base
from configs.app import app_settings
from db.connection import MongoConnection

logger = logging.getLogger(__name__)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(
    base.router,
    prefix='/base_route',
    tags=[
        'ws',
    ],
)


@app.on_event('startup')
async def startup_mongo():
    mongo_connection = MongoConnection(
        mongo_dsn=app_settings.MONGO_DSN,
    )
    await mongo_connection.init_mongo_db()


@app.on_event('shutdown')
async def shutdown_mongo():
    mongo_connection = MongoConnection(
        mongo_dsn=app_settings.MONGO_DSN,
    )
    mongo_connection.close()
