import datetime

from beanie import WriteRules
from fastapi import APIRouter
from memory_profiler import profile
from pydantic import BaseModel

from db.mongo_db.thread_document import ThreadDocument, CommentDocument

__all__ = ['router']


router = APIRouter()


@profile
@router.get('/detail', description='Get App Detail')
async def get_app_detail(thread_id: str):
    thread = await ThreadDocument.get(thread_id)
    await thread.fetch_all_links()
    return thread


class PostData(BaseModel):
    msg: str


@router.post('/detail', description='Get App Detail')
async def get_app_detail(data: PostData):
    dt_now = datetime.datetime.utcnow()
    thread = await ThreadDocument(
        updated_at=dt_now,
    ).insert()
    comments = []
    for i in range(1000):
        comment = CommentDocument(
            thread_id=thread.id,
            msg=data.msg,
            created_at=dt_now,
            updated_at=dt_now,
        )
        comments.append(comment)
    thread.comments.extend(comments)
    await thread.save(link_rule=WriteRules.WRITE)

    return thread
