IMAGE_NAME = reply_api:test
APP_PORT := $(or ${APP_PORT},${APP_PORT},5000)
APP_HOST := $(or ${APP_HOST},${APP_HOST},0.0.0.0)
APP_WORKER := $(or ${APP_WORKER},${APP_WORKER},1)
export PYTHONPATH := ./


.PHONY: test
test:
	ab -c 10 -n 1000 http://0.0.0.0:5000/base_route/detail?thread_id=65e1f105b784762fca1aeabb

.PHONY: run-app
run-app:
	@echo "Run APP ${ENV_FILE}"
	uvicorn --host ${APP_HOST} --port ${APP_PORT} --workers ${APP_WORKER} --reload api.main:app

.PHONY: run-local
run-local:
	CONFIG_ENV=test mprof run make run-app


.PHONY: clean
clean:
	@echo -n "Clear temp files"
	@echo -n "\n"
	@rm -rf `find . -name __pycache__`
	@rm -rf `find . -type f -name '*.py[co]' `
	@rm -rf `find . -type f -name '*~' `
	@rm -rf `find . -type f -name '.*~' `
	@rm -rf `find . -type f -name '@*' `
	@rm -rf `find . -type f -name '#*#' `
	@rm -rf `find . -type f -name '*.orig' `
	@rm -rf `find . -type f -name '*.rej' `
	@rm -rf .coverage
	@rm -rf coverage.html
	@rm -rf coverage.xml
	@rm -rf report.xml
	@rm -rf htmlcov
	@rm -rf build
	@rm -rf cover
	@rm -rf .develop
	@rm -rf .flake
	@rm -rf .install-deps
	@rm -rf *.egg-info
	@rm -rf .pytest_cache
	@rm -rf dist
